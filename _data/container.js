module.exports = {
  GAP: {
    phone: {"x": 12, "y": 12},
    tablet: {"x": 80, "y": 12},
    laptop: {"x": 80, "y": 12},
    desktop: {"x": 80, "y": 12},
    default: {"x": 80, "y": 12}
  },
  MAX_WIDTH: 1920,
}